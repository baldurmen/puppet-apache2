define apache2::conf (
  String $ensure = 'present',
) {

  if $ensure == 'present' {

    file {
    "/etc/apache2/conf-available/${title}.conf":
      ensure => file,
      source => "puppet:///files/apache2/confs/${title}.conf",
      owner  => 'root',
      group  => 0,
      mode   => '0644',
      notify => Exec["a2enconf-${title}"];
    }

    exec {
      "a2enconf-${title}":
        command => "/usr/sbin/a2enconf ${title}.conf",
        onlyif  => "/usr/bin/test ! -e /etc/apache2/conf-enabled/${title}.conf",
        require => File["/etc/apache2/conf-available/${title}.conf"],
        notify  => Service['apache2'];
    }
  }

  elsif $ensure == 'purged' {

    exec {
      "a2disconf-${title}":
        command => "/usr/sbin/a2disconf ${title}.conf",
        onlyif  => "/usr/bin/test -e /etc/apache2/conf-enabled/${title}.conf",
        notify  => Service['apache2'];
    }
  }

  else {
    fail("ensure value must either be 'present' or 'purged'")
  }
}
