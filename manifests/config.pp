class apache2::config {

  service {
    'apache2':
      ensure     => running,
      hasrestart => true,
      hasstatus  => true,
      require    => Package['apache2'];
  }
}
