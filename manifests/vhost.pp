define apache2::vhost (
  Optional[String] $document_root                      = undef,
  String $ensure                                       = 'present',
  String $vhost_conf                                   = "/etc/puppet/code/files/websites/vhosts/${title}.conf",
  Boolean $ssl                                         = true,
  Numeric $http_port                                   = 80,
  Numeric $https_port                                  = 443,
  Boolean $www                                         = false,
  Boolean $maintenance                                 = false,
  Optional[Stdlib::Unixpath] $maintenance_file         = '/var/www/maintenance.html',
  Optional[Stdlib::IP::Address] $maintenance_bypass_ip = '127.0.0.1',
) {

  if $ensure == 'present' {

    file {
      "/etc/apache2/sites-available/${title}.conf":
        ensure  => file,
        content => epp('apache2/vhost.conf.epp', {
          'document_root'         => $document_root,
          'ssl'                   => $ssl,
          'http_port'             => $http_port,
          'https_port'            => $https_port,
          'title'                 => $title,
          'vhost_conf'            => $vhost_conf,
          'www'                   => $www,
          'maintenance'           => $maintenance,
          'maintenance_file'      => $maintenance_file,
          'maintenance_bypass_ip' => $maintenance_bypass_ip,
        }),
        owner   => 'root',
        group   => 0,
        mode    => '0644',
        before  => Exec["a2ensite-${title}"],
        notify  => Service['apache2'];
    }

    if $ssl {

      exec {
        "a2ensite-${title}":
          command => "/usr/sbin/a2ensite ${title}.conf",
          onlyif  => "/usr/bin/test ! -e /etc/apache2/sites-enabled/${title}.conf",
          notify  => Service['apache2'],
          require => Letsencrypt::Certonly["$title"],
      }

      if $www == false {
        letsencrypt::certonly {
          "$title":
            plugin => 'apache',
        }
      }

      elsif $www {
        letsencrypt::certonly {
          "$title":
            domains => [ "$title", "www.${title}" ],
            plugin  => 'apache',
        }
      }
    }

    elsif $ssl == false {

      exec {
        "a2ensite-${title}":
          command => "/usr/sbin/a2ensite ${title}.conf",
          onlyif  => "/usr/bin/test ! -e /etc/apache2/sites-enabled/${title}.conf",
          notify  => Service['apache2'],
      }
    }
  }

  elsif $ensure == 'purged' {

    file {
      "/etc/apache2/sites-available/${title}.conf":
        ensure  => absent;
    }

    exec {
      "a2dissite-${title}":
        command => "/usr/sbin/a2dissite ${title}.conf",
        onlyif  => "/usr/bin/test -e /etc/apache2/sites-enabled/${title}.conf",
        before  => File["/etc/apache2/sites-available/${title}.conf"],
        notify  => Service['apache2'];
    }
  }

  else {
    fail("ensure value must either be 'present' or 'purged'")
  }
}
