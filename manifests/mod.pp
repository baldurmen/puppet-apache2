define apache2::mod (
  String $ensure = 'present',
) {

  if $ensure == 'present' {

    exec {
      "a2enmod-${title}":
        command => "/usr/sbin/a2enmod ${title}",
        onlyif  => "/usr/bin/test ! -L /etc/apache2/mods-enabled/${title}.load",
        notify  => Service['apache2'];
    }
  }

  elsif $ensure == 'purged' {

    exec {
      "a2dismod-${title}":
        command => "/usr/sbin/a2dismod ${title}",
        onlyif  => "/usr/bin/test -L /etc/apache2/mods-enabled/${title}.load",
        notify  => Service['apache2'];
    }
  }

  else {
    fail("ensure value must either be 'present' or 'purged'")
  }
}
